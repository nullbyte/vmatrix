import os
import term

fn main() {
	start()

	mut streams := []Stream{}

	// mut rl := readline.Readline{}
	// rl.enable_raw_mode_nosig()
	// rl.read_char()! == 113

	mut spawn_timer := Timer.new()
	spawn_timer.interval = 10

	for {
		columns, rows := term.get_terminal_size()
		// clear_terminal()
		if spawn_timer.update() {
			streams << Stream.new(columns)
		}
		for i, mut s in streams {
			if s.update(rows) {
				streams.delete(i)
			}
			s.draw()
		}
	}
}

fn start() {
	term.clear()
	os.signal_opt(.term, fn (s os.Signal) {
		stop()
	}) or {}
	os.signal_opt(.int, fn (s os.Signal) {
		stop()
	}) or {}
	// term.set_terminal_title('Welcome To The Matrix')
}

fn stop() {
	term.clear()
	exit(0)
}
