import rand
import term

const max_stream_len = 15
const min_stream_len = 3
const max_stream_speed = 80
const min_stream_speed = 50
const segment_characters = 'abcdefghijklmnopqrstuvwxyz1234567890()[]'

@[heap]
struct Stream {
	len   u8
	speed u8
	col   u16
mut:
	timer Timer
	head  u16
}

fn Stream.new(columns int) &Stream {
	mut s := &Stream{
		len:   u8(rand.u32_in_range(min_stream_len, max_stream_len) or { 0 })
		speed: u8(rand.u32_in_range(min_stream_speed, max_stream_speed) or { 0 })
		col:   u16(rand.u32n(u32(columns)) or { 0 })
		timer: Timer.new()
	}

	// s.timer.tick = fn [s] () { s.move }
	s.timer.interval = 100 - s.speed

	return s
}

fn (mut s Stream) move() {
	s.head++
}

fn (mut s Stream) update(rows int) bool {
	if s.timer.update() {
		s.head++
	}

	if int(s.head) - s.len > rows {
		s.clear()
		return true
	}
	return false
}

fn (s Stream) clear() {
	for i in 0 .. s.len {
		set_cursor_position(s.col, s.head - i)
		print(' ')
	}
}

fn (s Stream) draw() {
	set_cursor_position(s.col, s.head)
	print('a')
	for segment in 1 .. s.len {
		if s.head - segment < 0 {
			return
		}

		set_cursor_position(s.col, s.head - segment)
		r, g, b := s.get_segment_color(segment)
		mut chr := term.rgb(r, g, b, get_segment_character())

		if rand.int() % 2 == 0 {
			chr = term.bold(chr)
		}

		print(chr)
	}
	set_cursor_position(s.col, s.head - s.len - 1)
	print(' ')
}

fn (s Stream) get_segment_color(pos int) (int, int, int) {
	return 74 * (s.len - pos) / s.len, 255 * (s.len - pos) / s.len, 146 * (s.len - pos) / s.len
}

fn get_segment_character() string {
	i := rand.intn(segment_characters.len) or { 0 }
	return segment_characters[i].ascii_str()
}
