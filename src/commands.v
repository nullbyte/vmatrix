pub fn set_cursor_position(x int, y int) {
	print('\x1b[${y};${x}' + 'H')
}

fn clear_terminal() {
	print('\x1b[2J')
}
