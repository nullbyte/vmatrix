# VMatrix

A V implementation of [cmatrix](https://github.com/abishekvashok/cmatrix) for displaying text trails in your terminal like those seen in [The Matrix](https://en.wikipedia.org/wiki/The_Matrix)

### Improvements over cmatrix

- 24 bit colour (terminal support required)
- Characters change rather than remaining static
- Various speeds per stream

### Screenshot

<img src="screenshot.png">